package com.example.security.filter;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(value = "/allUsers")
public class GetAllUsers implements Filter {
    @Override
    public void init(FilterConfig filterConfig)  {}

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse res = (HttpServletResponse) response;
        final HttpSession session = req.getSession();

        // Проверка авторизирован ли пользователь
        if (session.getAttribute("login") != null && session.getAttribute("password") != null){
            req.getRequestDispatcher("/allUsers").forward(req,res);
        }
        else {
            res.sendRedirect("/security/logout");
        }

    }


    @Override
    public void destroy() {}
}
