package com.example.security.servlet;

import com.example.security.dao.UserDao;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
@WebServlet(value = "/allUsers")
public class ShowAllUsersServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        final HttpSession session = req.getSession();
        UserDao userDao = new UserDao();
        session.setAttribute("users", userDao.getAllUsers());
        getServletContext().getRequestDispatcher("/WEB-INF/view/all_user.jsp").forward(req, resp);
    }
}
