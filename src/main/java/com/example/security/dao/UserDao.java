package com.example.security.dao;

import com.example.security.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserDao {

    //database substitute
    private final List<User> list = new ArrayList<>();
    {
        list.add(new User(1, "Igor_login", "Igor", "1111", User.ROLE.USER));
        list.add(new User(2, "admin", "Admin", "0000", User.ROLE.USER));
        list.add(new User(3, "test", "test", "test", User.ROLE.USER));
        list.add(new User(4, "test1", "test1", "test1", User.ROLE.USER));
        list.add(new User(5, "test2", "test2", "test2", User.ROLE.USER));
    }

    public List<User> getAllUsers(){
        return list;
    }

    public boolean userExist(String login, String password) {
        boolean result = false;

        for (User user : list) {
            if (user.getLogin().equals(login) && user.getPassword().equals(password)) {
                result = true;
                break;
            }
        }

        return result;
    }

    public User getUserByLoginPassword(final String login, final String password) {

        User result = new User();
        result.setId(-1);

        for (User user : list) {
            if (user.getLogin().equals(login) && user.getPassword().equals(password)) {
                result = user;
            }
        }
        return result;
    }

}
